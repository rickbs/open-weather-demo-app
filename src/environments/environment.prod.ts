export const environment = {
  production: true,
  weatherApiApiKey: 'dd1614d687017547ad06e3b3cd644a87',
  weatherApiUrl: 'https://api.openweathermap.org/data/2.5',
  cities: [
    'Prague',
    'Bratislava',
    'Berlin',
    'Paris',
    'Rome',
  ]
};
