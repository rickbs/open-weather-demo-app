import { Component, OnDestroy, OnInit } from '@angular/core';

import { Plugins } from '@capacitor/core';
import { OverviewService } from '../../core/data/overview.service';
import { Observable, Subject } from 'rxjs';
import { WeatherObject } from '../../core/api/weather/weather.weather.interface';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { filter, switchMap, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-overview',
  templateUrl: 'overview.page.html',
  styleUrls: ['overview.page.scss'],
})
export class OverviewPage implements OnInit, OnDestroy {

  weathers$: Observable<WeatherObject[]> = this.overviewService.weatherObjects$;

  public destroyed = new Subject<any>();

  constructor(
    private overviewService: OverviewService,
    private router: Router
  ) {}

  ngOnInit() {
    const { App } = Plugins;
    const url = this.router.url;

     // This will update the data when the user navigates to this page
     this.router.events.pipe(
      filter((event: any) => event instanceof NavigationEnd),
      filter((event: RouterEvent) => event.url === url || event.url === '/'),
      switchMap(_ => this.refreshData()),
      takeUntil(this.destroyed)
    ).subscribe(_ => console.log('Updated data on navigation event'));

    // This will update the data when state is resumed from background on native devices
    App.addListener('appStateChange', (state) => {
      if (state.isActive && url === this.router.url) {
        this.refreshData().pipe(
          takeUntil(this.destroyed)
        ).subscribe(_ => console.log('updated data on resumed state'));
      }
    });
  }

  ngOnDestroy() {
    this.destroyed.next();
    this.destroyed.complete();
  }

  refreshData(): Observable<any> {
    return this.overviewService.updateData();
  }

  doRefresh(event: any) {
    this.refreshData().subscribe(
      _ => event.target.complete()
    );
  }

}
