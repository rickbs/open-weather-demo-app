import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { OverviewPage } from './overview.page';

import { OverviewPageRoutingModule } from './overview-routing.module';
import { WeatherListItemModule } from 'src/app/components/weather-list-item/weather-list-item.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OverviewPageRoutingModule,
    WeatherListItemModule
  ],
  declarations: [OverviewPage]
})
export class OverviewPageModule {}
