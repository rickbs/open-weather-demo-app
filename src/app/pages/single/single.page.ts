import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, RouterEvent } from '@angular/router';
import { Plugins } from '@capacitor/core';
import { Observable, Subject } from 'rxjs';
import { filter, switchMap, takeUntil } from 'rxjs/operators';
import { SingleWeatherForecast } from 'src/app/core/data/single.forecast.interface';
import { SingleService } from 'src/app/core/data/single.service';

@Component({
  selector: 'app-single',
  templateUrl: './single.page.html',
  styleUrls: ['./single.page.scss'],
})
export class SinglePage implements OnInit, OnDestroy {

  weather$: Observable<SingleWeatherForecast | null> = this.singleService.weatherObject$;
  public destroyed = new Subject<any>();

  constructor(
    private route: ActivatedRoute,
    private singleService: SingleService,
    private router: Router
  ) {}

  ngOnInit() {
    const { App } = Plugins;
    const url = this.router.url;
    const params = this.route.snapshot.params;
    const city = params.city;

    this.singleService.initialzeData(city);

     // This will update the data when the user navigates to this page
     this.router.events.pipe(
      filter((event: any) => event instanceof NavigationEnd),
      filter((event: RouterEvent) => event.url === url || event.url === '/'),
      switchMap(_ => this.refreshData()),
      takeUntil(this.destroyed)
    ).subscribe(_ => console.log('Updated data on navigation event'));

    // This will update the data when state is resumed from background on native devices
    App.addListener('appStateChange', (state) => {
      if (state.isActive && url === this.router.url) {
        this.refreshData().pipe(
          takeUntil(this.destroyed)
        ).subscribe(_ => console.log('updated data on resumed state'));
      }
    });
  }

  ngOnDestroy() {
    this.destroyed.next();
    this.destroyed.complete();
  }

  refreshData(): Observable<any> {
    return this.singleService.updateData();
  }

}
