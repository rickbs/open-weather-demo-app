import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SinglePageRoutingModule } from './single-routing.module';

import { SinglePage } from './single.page';
import { ForecastDailyModule } from 'src/app/components/forecast-daily/forecast-daily.module';
import { ForecastHourlyModule } from 'src/app/components/forecast-hourly/forecast-hourly.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SinglePageRoutingModule,
    ForecastDailyModule,
    ForecastHourlyModule
  ],
  declarations: [SinglePage]
})
export class SinglePageModule {}
