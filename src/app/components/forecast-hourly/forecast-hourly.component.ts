import { Component, Input } from '@angular/core';
import { Current } from 'src/app/core/api/weather/weather.onecall.interface';

@Component({
  selector: 'app-forecast-hourly',
  templateUrl: './forecast-hourly.component.html',
  styleUrls: ['./forecast-hourly.component.scss'],
})
export class ForecastHourlyComponent {

  @Input() forecast: Current[] | undefined;

}
