import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { ForecastHourlyComponent } from './forecast-hourly.component';

@NgModule({
  declarations: [ForecastHourlyComponent],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule,
  ],
  exports: [ForecastHourlyComponent]
})
export class ForecastHourlyModule { }
