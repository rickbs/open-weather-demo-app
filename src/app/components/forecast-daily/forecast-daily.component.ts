import { Component, Input } from '@angular/core';
import { Daily } from 'src/app/core/api/weather/weather.onecall.interface';

@Component({
  selector: 'app-forecast-daily',
  templateUrl: './forecast-daily.component.html',
  styleUrls: ['./forecast-daily.component.scss'],
})
export class ForecastDailyComponent {

  @Input()
  forecast: Daily[] = [];

}
