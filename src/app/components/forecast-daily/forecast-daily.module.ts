import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { ForecastDailyComponent } from './forecast-daily.component';

@NgModule({
  declarations: [ForecastDailyComponent],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule,
  ],
  exports: [ForecastDailyComponent]
})
export class ForecastDailyModule { }
