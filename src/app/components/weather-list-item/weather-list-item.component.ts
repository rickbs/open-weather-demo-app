import { Component, Input } from '@angular/core';
import { WeatherObject } from 'src/app/core/api/weather/weather.weather.interface';

@Component({
  selector: 'app-weather-list-item',
  templateUrl: './weather-list-item.component.html',
  styleUrls: ['./weather-list-item.component.scss'],
})
export class WeatherListItemComponent {

  @Input() weather: WeatherObject | undefined;

}
