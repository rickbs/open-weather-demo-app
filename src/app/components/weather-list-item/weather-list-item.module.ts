import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeatherListItemComponent } from './weather-list-item.component';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [WeatherListItemComponent],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule,
  ],
  exports: [WeatherListItemComponent],
})
export class WeatherListItemModule {}
