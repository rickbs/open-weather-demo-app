import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { getTestBed, TestBed } from '@angular/core/testing';
import { environment } from 'src/environments/environment';
import { UtilsService } from '../../utils/utils.service';

import { WeatherService } from './weather.api.service';
import { WeatherObject } from './weather.weather.interface';

describe('WeatherService', () => {
  let injector: TestBed;
  let service: WeatherService;
  let utilService: UtilsService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers:[UtilsService]
    });
    injector = getTestBed();
    service = injector.inject(WeatherService);
    utilService = injector.inject(UtilsService);
    httpMock = injector.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getWeatherByCityName', () => {
    it('should return an Observable<WeatherObject>', () => {
      const dummyWeatherObject: WeatherObject = {
        coord: {
          lon: 5,
          lat: 5
        },
        weather: [{
          main: 'Clear',
          icon: '',
          id: 56,
          description: 'test'
        }],
        base: 'string',
        main: {
          temp: 5,
          tempMax: 5,
          tempMin: 5,
          feelsLike: 6,
          pressure: 8,
          humidity: 10
        },
        visibility: 56,
        wind: {
          speed: 10,
          deg: 45
        },
        clouds: {
          all: 1
        },
        dt: 56,
        sys: {
          type: 4564,
          message: 456456,
          id: 5657,
          country: 'NL',
          sunrise: 123098120938,
          sunset: 123123234
        },
        timezone: 56,
        id: 56,
        name: 'string',
        cod: 56
      };

      const cityName = 'eindhoven';

      service.getWeatherByCityName(cityName).subscribe(weather => {
        expect(weather).toEqual(dummyWeatherObject);
      });

      // eslint-disable-next-line max-len
      const req = httpMock.expectOne(`${environment.weatherApiUrl}/weather?q=${cityName}&appid=${environment.weatherApiApiKey}&lang=${utilService.getDeviceLang()}&units=metric`);
      expect(req.request.method).toBe('GET');
      req.flush(dummyWeatherObject);
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
