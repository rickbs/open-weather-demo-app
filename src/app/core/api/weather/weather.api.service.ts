import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { UtilsService } from '../../utils/utils.service';
import { WeatherOneCallObject } from './weather.onecall.interface';
import { WeatherObject } from './weather.weather.interface';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  unit = 'metric';

  constructor(private http: HttpClient, private utilsService: UtilsService) { }

  getWeatherByCityName(name: string): Observable<WeatherObject> {
    return this.apiGet(`${environment.weatherApiUrl}/weather?q=${name}`) as Observable<WeatherObject>;
  }

  getWeatherByCityLatLng(lat: number, lng: number): Observable<WeatherObject> {
    return this.apiGet(`${environment.weatherApiUrl}/weather?lat=${lat}&lon=${lng}`) as Observable<WeatherObject>;
  }

  getWeatherByOneCall(lat: number, lng: number): Observable<WeatherOneCallObject> {
    return this.apiGet(`${environment.weatherApiUrl}/onecall?lat=${lat}&lon=${lng}&exclude=minutely`) as Observable<WeatherOneCallObject>;
  }

  private apiGet(url: string): Observable<WeatherObject|WeatherOneCallObject> {
    return this.http.get(`${url}&appid=${environment.weatherApiApiKey}&lang=${this.utilsService.getDeviceLang()}&units=${this.unit}`).pipe(
      map(res => this.utilsService.keysToCamel(res))
    );
  }
}
