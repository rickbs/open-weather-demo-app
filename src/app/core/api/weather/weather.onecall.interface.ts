export interface WeatherOneCallObject {
    lat:             number;
    lon:             number;
    timezone:        string;
    timezoneOffset:  number;
    current:         Current;
    hourly:          Current[];
    daily:           Daily[];
}

export interface Current {
    dt:         number;
    sunrise?:   number;
    sunset?:    number;
    temp:       number;
    feelsLike: number;
    pressure:   number;
    humidity:   number;
    dewPoint:  number;
    uvi:        number;
    clouds:     number;
    visibility: number;
    windSpeed: number;
    windDeg:   number;
    windGust:  number;
    weather:    Weather[];
    pop?:       number;
    rain?:      Rain;
}

export interface Rain {
    '1h': number;
}

export interface Weather {
    id:          number;
    main:        'Clear'|'Clouds'|'Rain'|'Snow'|'Extreme';
    description: string;
    icon:        string;
}

export interface Daily {
    dt:         number;
    sunrise:    number;
    sunset:     number;
    moonrise:   number;
    moonset:    number;
    moonPhase: number;
    temp:       Temp;
    feelsLike: FeelsLike;
    pressure:   number;
    humidity:   number;
    dewPoint:  number;
    windSpeed: number;
    windDeg:   number;
    windGust?: number;
    weather:    Weather[];
    clouds:     number;
    pop:        number;
    uvi:        number;
    rain?:      number;
}

export interface FeelsLike {
    day:   number;
    night: number;
    eve:   number;
    morn:  number;
}

export interface Temp {
    day:   number;
    min:   number;
    max:   number;
    night: number;
    eve:   number;
    morn:  number;
}
