import { Injectable } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import { distinctUntilChanged, filter, map, mergeMap, tap } from 'rxjs/operators';
import { WeatherService } from '../api/weather/weather.api.service';
import { SingleWeatherForecast } from './single.forecast.interface';

@Injectable({
  providedIn: 'root'
})
export class SingleService {

  private _weatherObject = new BehaviorSubject<SingleWeatherForecast | null>(null);
  private _activeCity = '';

  public readonly weatherObject$: Observable<SingleWeatherForecast | null> =
    this._weatherObject.asObservable().pipe(
      filter(res => res !== null),
      distinctUntilChanged()
    );

  constructor(private weatherApiService: WeatherService) { }

  async initialzeData(city: string) {
    if(this._activeCity === city) {
      return;
    }

    this._weatherObject.next(null);
    this._activeCity = city;
  }

  updateData(): Observable<SingleWeatherForecast> {
    return this.retrieveDataFromApi(this._activeCity).pipe(
      tap(data => console.log('weather objects updated', data))
    );
  }

  private retrieveDataFromApi(city: string): Observable<SingleWeatherForecast> {
    return this.weatherApiService.getWeatherByCityName(city).pipe(
      mergeMap(res => combineLatest([
        of(res),
        this.weatherApiService.getWeatherByOneCall(res.coord.lat, res.coord.lon)
      ])),
      map(([current, forecast]) => ({ current, forecast }) ),
      map(res => this.roundTemps(res)),
      tap(res => this._weatherObject.next(res)),
    );
  }

  private roundTemps(forecast: SingleWeatherForecast): SingleWeatherForecast {
    forecast.current.main.temp = Math.round(forecast.current.main.temp);
    forecast.current.main.feelsLike = Math.round(forecast.current.main.feelsLike);
    forecast.current.main.tempMin = Math.round(forecast.current.main.tempMin);
    forecast.current.main.tempMax = Math.round(forecast.current.main.tempMax);

    forecast.forecast.daily.forEach(daily => {
      daily.temp.day = Math.round(daily.temp.day);
      daily.temp.min = Math.round(daily.temp.min);
      daily.temp.max = Math.round(daily.temp.max);
      daily.temp.eve = Math.round(daily.temp.eve);
      daily.temp.morn = Math.round(daily.temp.morn);
      daily.temp.night = Math.round(daily.temp.night);
    });

    forecast.forecast.hourly.forEach(hourly => {
      hourly.temp = Math.round(hourly.temp);
    });

    return forecast;
  }

}
