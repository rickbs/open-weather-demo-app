/* eslint-disable max-len */
import { TestBed } from '@angular/core/testing';
import { WeatherService } from '../api/weather/weather.api.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { OverviewService } from './overview.service';
import { WeatherObject } from '../api/weather/weather.weather.interface';

describe('OverviewService', () => {
  let service: OverviewService;

  const dummyWeatherObject: WeatherObject[] = [
    {coord:{lon:-85.1647,lat:34.235},weather:[{id:800,main:'Clear',description:'clear sky',icon:'01d'}],base:'stations',main:{temp:26.76,feelsLike:26.43,tempMin:26.11,tempMax:27.22,pressure:1018,humidity:34},visibility:10000,wind:{speed:3.09,deg:140},clouds:{all:1},dt:1619471319,sys:{type:1,id:5680,country:'US',sunrise:1619434557,sunset:1619482833},timezone:-14400,id:4219762,name:'Rome',cod:200,},
    {coord:{lon:-83.1623,lat:34.257},weather:[{id:800,main:'Rain',description:'rainy',icon:'01d'}],base:'stations',main:{temp:26.76,feelsLike:26.43,tempMin:26.11,tempMax:27.22,pressure:1018,humidity:34},visibility:10000,wind:{speed:3.09,deg:140},clouds:{all:1},dt:1619471319,sys:{type:1,id:5680,country:'US',sunrise:1619434557,sunset:1619482833},timezone:-14400,id:4219762,name:'Rome',cod:200,}
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [WeatherService]
    });
    service = TestBed.inject(OverviewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('updateData', () => {
    it('should return an Observable<WeatherObject[]>', () => {
     service.updateData().subscribe(weatherObject => {
        expect(weatherObject.length).toBe(2);
        expect(weatherObject).toEqual(dummyWeatherObject);
      });
    });
  });
});
