/* eslint-disable max-len */
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { WeatherService } from '../api/weather/weather.api.service';
import { SingleWeatherForecast } from './single.forecast.interface';

import { SingleService } from './single.service';

describe('SingleService', () => {
  let service: SingleService;

  const dummyWeatherObject: SingleWeatherForecast = {
    current: {coord:{lon:-85.1647,lat:34.257},weather:[{id:800,main:'Clear',description:'clear sky',icon:'01d'}],base:'stations',main:{temp:26.76,feelsLike:26.43,tempMin:26.11,tempMax:27.22,pressure:1018,humidity:34},visibility:10000,wind:{speed:3.09,deg:140},clouds:{all:1},dt:1619471319,sys:{type:1,id:5680,country:'US',sunrise:1619434557,sunset:1619482833},timezone:-14400,id:4219762,name:'Rome',cod:200},
    forecast: {lat: 51.45,lon: 5.4667,timezone: 'Europe/Amsterdam',timezoneOffset: 7200,current: {  dt: 1619474034,  sunrise: 1619410765,  sunset: 1619463126,  temp: 7.45,  feelsLike: 7.45,  pressure: 1013,  humidity: 57,  dewPoint: -0.45,  uvi: 0,  clouds: 94,  visibility: 10000,  windSpeed: 0.45,  windDeg: 29,  windGust: 5.36,  weather: [    {      id: 804,      main: 'Clouds',      description: 'overcast clouds',      icon: '04n'    }  ]},hourly: [  {    dt: 1619470800,    temp: 7.28,    feelsLike: 4.34,    pressure: 1014,    humidity: 58,    dewPoint: -0.38,    uvi: 0,    clouds: 94,    visibility: 10000,    windSpeed: 4.66,    windDeg: 39,    windGust: 10.27,    weather: [      {        id: 804,        main: 'Clouds',        description: 'overcast clouds',        icon: '04n'      }    ],    pop: 0  },],daily: [  {    dt: 1619434800,    sunrise: 1619410765,    sunset: 1619463126,    moonrise: 1619460240,    moonset: 1619410980,    moonPhase: 0.47,    temp: {      day: 11.63,      min: 1.47,      max: 13.12,      night: 7.28,      eve: 11.66,      morn: 1.66    },    feelsLike: {      day: 9.87,      night: -1.2,      eve: 10.03,      morn: -1.2    },    pressure: 1021,    humidity: 39,    dewPoint: -1.76,    windSpeed: 4.76,    windDeg: 47,    windGust: 10.27,    weather: [      {        id: 800,        main: 'Clear',        description: 'clear sky',        icon: '01d'      }    ],    clouds: 1,    pop: 0,    uvi: 4.98  },]        }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [WeatherService]
    });
    service = TestBed.inject(SingleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('weatherObject$', () => {
    it('should return an Observable<SingleWeatherForecast>', () => {
      service.weatherObject$.subscribe(weatherObject => {
        expect(weatherObject).toEqual(dummyWeatherObject);
      });
    });
  });

  describe('updateData', () => {
    it('should return an Observable<SingleWeatherForecast>', () => {
     service.updateData().subscribe(weatherObject => {
        expect(weatherObject).toEqual(dummyWeatherObject);
      });
    });
  });
});
