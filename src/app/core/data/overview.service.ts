import { Injectable } from '@angular/core';
import { PermissionType, Plugins } from '@capacitor/core';
import { BehaviorSubject, forkJoin, from, Observable, of, zip } from 'rxjs';
import { distinctUntilChanged, filter, map, switchMap, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { WeatherService } from '../api/weather/weather.api.service';
import { WeatherObject } from '../api/weather/weather.weather.interface';

@Injectable({
  providedIn: 'root'
})
export class OverviewService {

  private _weatherObjects = new BehaviorSubject<WeatherObject[]>([]);

  public readonly weatherObjects$: Observable<WeatherObject[]> = this._weatherObjects.asObservable().pipe(
    filter(res => res !== []),
    distinctUntilChanged()
  );

  constructor(private weatherApiService: WeatherService) { }

  updateData(): Observable<WeatherObject[]> {
    return this.retrieveDataFromApi().pipe(
      tap(data => console.log('weather objects updated', data))
    );
  }

  private retrieveDataFromApi(): Observable<WeatherObject[]> {
    const weatherObservables = [];

    environment.cities.forEach(city => weatherObservables.push(
      this.weatherApiService.getWeatherByCityName(city)
    ));

    weatherObservables.unshift(this.retrieveDataFromUserLocation());

    return forkJoin(weatherObservables).pipe(
      map(res => res.filter(r => r !== null) as WeatherObject[]),
      map((res: WeatherObject[]) => {
        res.forEach(result => {
          result.main.temp = Math.round(result.main.temp);
        });

        return res;
      }),
      tap(res => this._weatherObjects.next(res))
    );
  }

  private retrieveDataFromUserLocation(): Observable<WeatherObject | null> {
    return from(this.getCoordinatesFromGeolocation()).pipe(
      switchMap(res => {
        const latitude = res?.latitude;
        const longitude = res?.longitude;

        if(latitude && longitude) {
          return this.weatherApiService.getWeatherByCityLatLng(latitude, longitude);
        }

        return of(null);
      })
    );
  }

  private async getCoordinatesFromGeolocation() {
    const { Geolocation, Permissions } = Plugins;

    const permission = await Permissions.query({
      name: PermissionType.Geolocation
    });

    if(permission.state === 'granted' || permission.state === 'prompt') {
      try {
        const geolocation = await Geolocation.getCurrentPosition();
        return geolocation.coords;
      } catch(error) {
        return null;
      }
    } else {
      return null;
    }
  }

}
