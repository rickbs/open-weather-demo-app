import { WeatherOneCallObject } from '../api/weather/weather.onecall.interface';
import { WeatherObject } from '../api/weather/weather.weather.interface';

export interface SingleWeatherForecast {
    current: WeatherObject;
    forecast: WeatherOneCallObject;
}
