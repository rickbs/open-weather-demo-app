import { Injectable } from '@angular/core';

interface Navigator {
  userLanguage?: string;
  browserLanguage?: string;
  languages: readonly string[];
  language: string;
}

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  getDeviceLang(): string {
    const navigator: Navigator = window.navigator;
    return ((navigator.language || navigator.browserLanguage || navigator.userLanguage) ?? 'en').substring(0, 2);
  }

  // Function from:
  // https://stackoverflow.com/questions/56568423/typescript-no-index-signature-with-a-parameter-of-type-string-was-found-on-ty
  keysToCamel(o: any): any {
    if (o === Object(o) && !Array.isArray(o) && typeof o !== 'function') {
      const n = {};
      Object.keys(o).forEach((k) => n[this.toCamel(k)] = this.keysToCamel(o[k]));
      return n;
    } else if (Array.isArray(o)) {
      return o.map((i) =>  this.keysToCamel(i));
    }
    return o;
  }

  // Function from:
  // https://stackoverflow.com/questions/56568423/typescript-no-index-signature-with-a-parameter-of-type-string-was-found-on-ty
  private toCamel(s: string): string {
    return s.replace(/([-_][a-z])/ig, ($1) => $1.toUpperCase().replace('-', '').replace('_', ''));
  }
}
