import { TestBed } from '@angular/core/testing';

import { UtilsService } from './utils.service';

describe('UtilsService', () => {
  let service: UtilsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UtilsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('deviceLang should be string of 2 chars', () => {
    expect(service.getDeviceLang().length).toBe(2);
  });

  it('should convert json with _\'s to camelCasing', () => {
    const json = {
      t_a: 15,
      complex_object: [{
        id: 0,
        complex_number: 1
      }]
    };

    const expectedJson = {
      tA: 15,
      complexObject: [{
        id: 0,
        complexNumber: 1
      }]
    };

    expect(JSON.stringify(service.keysToCamel(json)) === JSON.stringify(expectedJson)).toBeTrue();
  });
});
