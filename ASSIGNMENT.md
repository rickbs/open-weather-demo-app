Assignment 
• Please create a readme file that explains what you have done
• Please keep it to a minimum in leveraging/using scaffolding tools/boilerplate templates/plugins.
• Please apply unit tests

You should use Open Weather Data to create a single page application that presents a list of 5 European cities of your choice.

Your goal is to get the current weather situation displaying the city name plus average temperature and the wind strength.

Clicking on an item shows the forecast in the next hours.

Try creating something simple, user friendly and eye appealing. Feel free to use any UI resources you’d like to.

Focus on clean, reusable code and best practices.

Using Ionic and Angular framework is a must.

Open Weather Data API
Please read the details about the Open Weather API here: http://openweathermap.org/current

Once done, please create a public GitHub repo and share the link with us.
All the best!