[![pipeline status](https://gitlab.com/rickbs/open-weather-demo-app/badges/master/pipeline.svg)](https://gitlab.com/rickbs/open-weather-demo-app/-/commits/master)
[![coverage report](https://gitlab.com/rickbs/open-weather-demo-app/badges/master/coverage.svg)](https://gitlab.com/rickbs/open-weather-demo-app/-/commits/master)

# Open Weather Demo App

## What I have done

I've created a very simple app that will display weather information for 5 cities and your current location if geolocation is active.

Just some of the things I did here:
- Added console manipulation for production
- Added more strict rules for tsconfig.json
- Created a mapping service for the api
- Created a data service to handle all data for the overview page
- Created a data service to handle all data for the single page
- Implemented all logic to handle observables including refreshing data when on native devices
- Implemented some geolocation to track local weather for fun
- Implemented more logic to combine observables into one observable per page
- Added some visuals to the pages so have an actual app instead of raw data
- Added some custom fonts to benefit more of the design
- Cross-checked dark mode to be consistent like light mode.
- Added some gitlab-ci to run unit tests
- Added code coverage to the gitlab project
- Added some unit testing 

## What more could be done

Apps like these are never really done so I just made a list of some ideas how to improve this application even further.
- Handle possible errors
- Add data storage so we don't have to do this many API requests all the time
- Hide API key from public repository
- Setup automated build scripts for Android/iOS/Web
- Ask user for permission before requesting geolocation data
- Handle offline support 
- Combine forecast-hourly and forecast-daily into one component with the same data structure.